Drop Database IF EXISTS Ejercicio1;

Create Database Ejercicio1;
Use Ejercicio1;


Create Table Fabricantes(CIF INT PRIMARY KEY, DomicilioSocial VARCHAR(20));

Create Table Componente(Nombre VARCHAR(20) PRIMARY KEY, Especificaciones VARCHAR(20), 
        CIF INT, FOREIGN KEY(CIF) references Fabricantes(CIF));

Create Table Electrodomésticos(Características VARCHAR(20), Nombre VARCHAR(20), 
    PRIMARY KEY(Características, Nombre));

/*Falta la relación reflexiva de Electrodomésticos*/

Create Table Aparato_Electrónico(Código INT PRIMARY KEY, Descripción VARCHAR(20), 
    CaracterísticasELEC VARCHAR(20), 
    FOREIGN KEY(CaracterísticasELEC) references Electrodomésticos(Características));
    
Create Table creado(CódigoAPA_ELEC INT, NombreCOMP VARCHAR(20), Cantidad INT,
    FOREIGN KEY(CódigoAPA_ELEC) references Aparato_Electrónico(Código),
    FOREIGN KEY(NombreCOMP) references Componente(Nombre));

Show Tables;
