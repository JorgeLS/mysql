Drop Database IF EXISTS Ej2Refuerzo; /* STAR WARS*/

Create Database Ej2Refuerzo;
Use Ej2Refuerzo;

Create Table ACTORES(Codigo INT AUTO_INCREMENT, Nombre VARCHAR(30), Fecha DATE,
    Nacionalidad VARCHAR(30) DEFAULT 'Estadounidense',
    CONSTRAINT PK_ACTORES PRIMARY KEY(Codigo));

Create Table PERSONAJES(Codigo INT AUTO_INCREMENT, Nombre VARCHAR(30),
    Raza VARCHAR(30) DEFAULT 'Humano', Grado SMALLINT UNSIGNED,
    Codigo_ACTORES INT, CodigoSuperior_PERSONAJES INT,
    CONSTRAINT PK_PERSONAJES PRIMARY KEY(Codigo),
    CONSTRAINT FK_ACTORES_PERSONAJES FOREIGN KEY(Codigo_ACTORES) REFERENCES ACTORES(Codigo),
    CONSTRAINT FK_PERSONAJES_PERSONAJES FOREIGN KEY(CodigoSuperior_PERSONAJES)
    REFERENCES PERSONAJES(Codigo));

Create Table PLANETAS(Codigo INT AUTO_INCREMENT, Galaxia VARCHAR(20), Nombre Varchar(30),
    CONSTRAINT PK_PLANETAS PRIMARY KEY(Codigo));

Create Table PELICULAS(Codigo INT AUTO_INCREMENT, Titulo VARCHAR(50),
    Director VARCHAR(30), Año YEAR(4),
    CONSTRAINT PK_PELICULAS PRIMARY KEY(Codigo));

Create Table PERSONAJESPELICULAS(Codigo_PERSONAJES INT, Codigo_PELICULAS INT,
    CONSTRAINT FK_PERSONAJES_PERSONAJESPELICULAS FOREIGN KEY(Codigo_PERSONAJES)
    REFERENCES PERSONAJES(Codigo),
    CONSTRAINT FK_PELICULAS_PERSONAJESPELICULAS FOREIGN KEY(Codigo_PELICULAS)
    REFERENCES PELICULAS(Codigo));

Create Table NAVES(Codigo INT AUTO_INCREMENT, NºTripulantes SMALLINT, Nombre VARCHAR(20),
    CONSTRAINT PK_NAVES PRIMARY KEY(Codigo));

Create Table VISITAS(Codigo_NAVES INT, Codigo_PLANETAS INT, Codigo_PELICULAS INT,
    CONSTRAINT FK_NAVES_VISITAS FOREIGN KEY(Codigo_NAVES) REFERENCES NAVES(Codigo),
    CONSTRAINT FK_PLANETAS_VISITAS FOREIGN KEY(Codigo_PLANETAS) REFERENCES PLANETAS(Codigo),
    CONSTRAINT FK_PELICULAS_VISITAS FOREIGN KEY(Codigo_PELICULAS) REFERENCES PELICULAS(Codigo));

Show Tables;

source ~/pepe/work/mysql/ValuesEj2Refuerzo.sql
