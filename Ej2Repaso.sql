
CREATE TABLE tab1(
    col1 INT(5) PRIMARY KEY,
    col2 VARCHAR(25) NOT NULL PRIMARY KEY,
    col3 INT,
    col4 VARCHAR(10),
    CONSTRAINT fk_tab2_tab1 FOREIGN KEY (col3, col4) REFERENCES tab2(col1, col2)
);
