Drop Database IF EXISTS Ejercicio3;

Create Database Ejercicio3;
Use Ejercicio3;

Create Table Mueble(Nombre VARCHAR(20) PRIMARY KEY, Precio DECIMAL(4,2));
Create Table Piezas(Nombre VARCHAR(20) PRIMARY KEY);
Create Table Estantes(Pasillo INT, Altura DECIMAL(4,2), PRIMARY KEY(Pasillo, Altura));
Create Table formado(NombreMUEBLE VARCHAR(20), NombrePIEZAS VARCHAR(20), Cantidad INT,
        FOREIGN KEY(NombreMUEBLE) references Mueble(Nombre),
        FOREIGN KEY(NombrePIEZAS) references Piezas(Nombre));
Create Table están(NombrePIEZAS VARCHAR(20), PasilloEST INT, AlturaEST DECIMAL(3,2),
        Cantidad INT,
        FOREIGN KEY(NombrePIEZAS) references Piezas(Nombre),
        FOREIGN KEY(PasilloEST) references Estantes(Pasillo),
        FOREIGN KEY(ALturaEST) references Estantes(Altura));
    /*Me pone lo de Constraint*/

Show Tables;
