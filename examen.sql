DROP DATABASE IF EXISTS ExamenStarkTrek;

CREATE Database ExamenStarkTrek;
USE ExamenStarkTrek;

CREATE TABLE ACTORES (
    Nombre VARCHAR(20),
    Personaje VARCHAR(30),
    FechaNacimiento DATE,
    Nacionalidad VARCHAR(20),

    CONSTRAINT PK_ACTORES PRIMARY KEY(Nombre)
);

CREATE TABLE PERSONAJES (
    Nombre VARCHAR(30) UNIQUE,
    Raza VARCHAR(20),
    GraduacionMilitar ENUM('Capitán', 'Teniente', 'Almirante', 'Oficial'),
    NombreActor VARCHAR(20),
    NombrePersonajes VARCHAR(30),

    CONSTRAINT PK_PERSONAJES PRIMARY KEY (Nombre),
    CONSTRAINT FK_ACTORES_PERSONAJES FOREIGN KEY (NombreActor) REFERENCES ACTORES(Nombre),
    CONSTRAINT FK_PERSONAJES_PERSONAJES FOREIGN KEY (NombrePersonajes) REFERENCES PERSONAJES(Nombre)
);

CREATE TABLE NAVES (
    Nombre VARCHAR(30) UNIQUE,
    Codigo INT UNSIGNED,
    NumTripulantes INT UNSIGNED,

    CONSTRAINT PK_NAVES PRIMARY KEY (Codigo)
);

CREATE TABLE PLANETAS (
    Codigo INT UNSIGNED,
    Nombre VARCHAR(30),
    Galaxia VARCHAR(40),
    Problema VARCHAR(255),
    CodigoNave INT UNSIGNED,

    CONSTRAINT PK_PLANETAS PRIMARY KEY (Codigo),
    CONSTRAINT FK_NAVES_PLANETAS FOREIGN KEY (CodigoNave) REFERENCES NAVES(Codigo)
);

CREATE TABLE CAPITULOS (
    Numero INT UNSIGNED,
    Temporada INT UNSIGNED,
    Titulo VARCHAR(40) UNIQUE,
    Orden INT UNSIGNED,
    CodigoPlaneta INT UNSIGNED,

    CONSTRAINT PK_CAPITULOS PRIMARY KEY (Numero, Temporada),
    CONSTRAINT FK_PLANETAS_CAPITULOS FOREIGN KEY (CodigoPlaneta) REFERENCES PLANETAS(Codigo)
);

CREATE TABLE APARICIONSerie (
    NombrePersonaje VARCHAR(30),
    NumeroCapitulo INT UNSIGNED,
    NumeroTemporada INT UNSIGNED,

    CONSTRAINT FK_PERSONAJES_APARICIONSerie FOREIGN KEY (NombrePersonaje) REFERENCES PERSONAJES(Nombre),
    CONSTRAINT FK_CAPITULO_APARICIONSerie FOREIGN KEY (NumeroCapitulo, NumeroTemporada) REFERENCES CAPITULOS(Numero, Temporada)
);

CREATE TABLE PELICULAS (
    Titulo VARCHAR(30),
    AñoLanzamiento YEAR,
    Director VARCHAR(50),

    CONSTRAINT PK_PELICULAS PRIMARY KEY (Titulo)
);

CREATE TABLE APARICIONPeliculas (
    NombrePersonaje VARCHAR(30),
    TituloPELICULA VARCHAR(30),
    Protagonista ENUM('Si', 'No'),

    CONSTRAINT FK_PERSONAJES_APARICIONPeliculas FOREIGN KEY (NombrePersonaje) REFERENCES PERSONAJES(Nombre),
    CONSTRAINT FK_PELICULA_APARICIONPeliculas FOREIGN KEY (TituloPELICULA) REFERENCES PELICULAS(Titulo)
);

/* Modifica una columna */
ALTER TABLE CAPITULOS DROP FOREIGN KEY FK_PLANETAS_CAPITULOS;
ALTER TABLE PLANETAS MODIFY Codigo INT UNSIGNED AUTO_INCREMENT;
ALTER TABLE CAPITULOS ADD CONSTRAINT FK_PLANETAS_CAPITULOS FOREIGN KEY (CodigoPlaneta) REFERENCES PLANETAS(Codigo);

/* Actualizacion de una foreign key */
ALTER TABLE CAPITULOS DROP FOREIGN KEY FK_PLANETAS_CAPITULOS;
ALTER TABLE CAPITULOS ADD CONSTRAINT FK_PLANETAS_CAPITULOS FOREIGN KEY (CodigoPlaneta) REFERENCES PLANETAS(Codigo) ON UPDATE CASCADE;

/* Añade una Columna Recaudación en Peliculas */
ALTER TABLE PELICULAS ADD Recaudacion DECIMAL(5,2);

/* Insert Values */
INSERT INTO ACTORES VALUES ('Luke Perry', 'SpiderCerdo', '1990-01-01', 'Turca');
INSERT INTO ACTORES VALUES ('Galindo Polindo', 'Ant-Man', '1995-11-21', 'Marciano');
INSERT INTO PERSONAJES VALUES ('SpiderCerdo', 'Mutante', 'Oficial', 'Luke Perry', NULL);
INSERT INTO NAVES VALUES ('Andromeda', 1, 20);
INSERT INTO PLANETAS(Nombre,Galaxia,Problema,CodigoNave) VALUES ('Kamino', 'Via Lactea', 'Nos enfrentamos mi Capitan, mi Capitan...', 1);
INSERT INTO CAPITULOS VALUES (1, 1, 'Begining is the End is the Beginning', 3, 1);
INSERT INTO CAPITULOS VALUES (1, 2, 'StaRkTrEcK2x01', 3, 1);
INSERT INTO APARICIONSerie VALUES ('SpiderCerdo', 1, 1);
INSERT INTO PELICULAS VALUES ('Shin-Chan y las bolas Mágicas', 2007, 'Nikito Nipongo LaPelota', 900.5);
INSERT INTO APARICIONPeliculas VALUES ('SpiderCerdo', 'Shin-Chan y las bolas Mágicas', 'Si');

/* Actualiza un dato */
UPDATE APARICIONSerie SET NumeroTemporada='2' WHERE NumeroCapitulo=1 AND NumeroTemporada=1;

/* Borra Cualquier Dato Tabla Actores */
DELETE FROM ACTORES WHERE Nacionalidad='Turca'; /* Aquí no se puede borrar porque esta referenciado, ni en cascada */
DELETE FROM ACTORES WHERE Nacionalidad='Marciano'; /* Aquí se puede borrar porque no esta referenciado, ni en cascada */

/* Borra Tabla Actores */
DROP TABLE ACTROES /* NO se puede porque dependen otras tablas de él */

/* USUARIOS PON ALGO */
SHOW TABLES;

