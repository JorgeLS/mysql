Drop Database IF EXISTS RepasoEj1;

Create Database RepasoEj1;
Use RepasoEj1;

Create Table Persona(Nombre VARCHAR(20), Apellidos VARCHAR(40), Trabajo VARCHAR(50),
      PRIMARY KEY(Nombre, Apellidos));
Create Table Objeto(Nombre VARCHAR(20), Tamaño ENUM('pequeño','mediano','grande'), 
      NombreOBJETOcontenedor VARCHAR(20),
      PRIMARY KEY(Nombre),
      FOREIGN KEY(NombreOBJETOcontenedor) references Objeto(Nombre));
Create Table Situaciones(Hora TINYINT UNSIGNED, Lugar VARCHAR(30), NombrePERSONA VARCHAR(20), 
      ApellidosPERSONA VARCHAR(40), Vestuario VARCHAR(50), Mercancia VARCHAR(20),
      PRIMARY KEY(Hora), 
      FOREIGN KEY(NombrePERSONA, ApellidosPERSONA) references Persona(Nombre, Apellidos));

Create Table lleva(HoraSIT TINYINT UNSIGNED, NombreOBJ VARCHAR(20), 
      FOREIGN KEY(HoraSIT) references Situaciones(Hora),
      FOREIGN KEY(NombreOBJ) references Objeto(Nombre));

Show Tables;

Insert INTO Persona VALUES('Skyler','White','Abogado'),
                          ('Jessie','Pinkman','Dealer'),
                          ('Saul','Goodman','Contable');
Insert INTO Objeto VALUES('Coche','grande',NULL),
                         ('Mochila','mediano','Coche'),
                         ('Pistola','pequeño','Mochila');
Insert INTO Situaciones VALUES(9,'Caravana','Jessie','Pinkman','Heisenberg','Baby Blue'),
                              (10,'Despacho','Saul','Goodman','Traje','Pasta $$$'),
                              (20,'Casa','Jessie','Pinkman','Domingo',NULL);
Insert INTO lleva VALUES(9,'Pistola'),
                        (10,'Mochila'),
                        (20,NULL);

Select * From Persona;
Select * From Objeto;
Select * From Situaciones;
Select * From lleva;
