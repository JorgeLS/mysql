Drop Database IF EXISTS Ejercicio5;

Create Database Ejercicio5;
Use Ejercicio5;

Create Table Ciudad(Código Varchar(20) Primary key, Nombre Varchar(20));
Create Table Temperatura(Día Date, Código_Ciu Varchar(20), Grados Int,
                        Primary key(Día),
                        Foreign key(Código_Ciu) references Cuidad(Código));
Create Table Humedad(Día Date, Código_Ciu Varchar(20), Porcentaje int,
                        Primary key(Día),
                        Foreign key(Código_Ciu) references Cuidad(Código));
Create Table Informe(ID int AUTO_INCREMENT, Día date, Código_Ciu Varchar(20),
                        Primary key(ID),
                        Foreign key(Código_Ciu) references Cuidad(Código));

Show Tables;
/*
Insert INTO Ciudad VALUES();
Insert INTO Temperatura VALUES();
Insert INTO Humedad VALUES();
Insert INTO Informe VALUES();
*/
Select * FROM Ciudad;
Select * FROM Temperatura;
Select * FROM Humedad;
Select * FROM Informe;
