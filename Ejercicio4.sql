Drop Database IF EXISTS Ejercicio4;

Create Database Ejercicio4;
Use Ejercicio4;

Create Table Personaje(Nombre VARCHAR(20), Fuerza INT, Inteligencia INT, Habilidad INT,
    NombPERS_Dominador VARCHAR(20), NúmeroESC INT,
    FOREIGN KEY(NúmeroESC) references Escenario(Número),
    FOREIGN KEY(NombPERS_Dominador) references Personaje(NombPERS_Dominador));
/*No se Hacer lo del atributo reflexivo*/

Create Table Objeto(Código INT PRIMARY KEY, Descripción VARCHAR(20), NúmeroESC INT,
    NombrePERS VARCHAR(20), Tiempo TIME,          
    FOREIGN KEY(NúmeroESC) references Escenario(Número),
    FOREIGN KEY(NombrePERS) references Personaje(Nombre));

Create Table Escenario(Número INT, Tiempo TIME, Riesgo VARCHAR(20));

Show Tables;
