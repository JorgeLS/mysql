Drop Database IF EXISTS RepasoEj3;

Create Database RepasoEj3;
Use RepasoEj3;

Create Table Contacto(ID INT AUTO_INCREMENT, Nombre VARCHAR(30), Apellidos VARCHAR(30),
                     PRIMARY KEY(ID));
Create Table Famoso(ID_CONTACTO INT, NºFamosos INT, Apodo VARCHAR(20),
                   FOREIGN KEY(ID_CONTACTO) references Contacto(ID));
Create Table Realeza(ID_CONTACTO INT, Ocupación ENUM('rey','reina','princesa'),
                    FOREIGN KEY(ID_CONTACTO) references Contacto(ID));
Create Table Político(ID_CONTACTO INT, NºRelevacia VARCHAR(20), 
                     PartidoPolítico VARCHAR(20),
                     FOREIGN KEY(ID_CONTACTO) references Contacto(ID));
Create Table Evento(ID_CONTACTO INT, Lugar VARCHAR(20), Hora TINYINT UNSIGNED, 
                   Fecha DATE, Cargo VARCHAR(20),
                   FOREIGN KEY(ID_CONTACTO) references Contacto(ID));

Create Table presentan(ID_CONTACTOpresentado INT, 
                      ID_CONTACTOpresentador INT,
                      FOREIGN KEY(ID_CONTACTOpresentado) references Contacto(ID),
                      FOREIGN KEY(ID_CONTACTOpresentador) references Contacto(ID));

Show Tables;

Insert INTO Contacto(Nombre, Apellidos) VALUES('Felipe', 'Borbón'),
                                              ('Iker', 'Casillas'),
                                              ('Pablo','Iglesias'),
                                              ('Pedro','Sanchez'),
                                              ('Iñigo','Matatoros'),
                                              ('Leticia','de Borbón'),
                                              ('Sofía','de Borbón');
Insert INTO Famoso VALUES(1,'614041931','El Alto'),
                         (2,'647821593','Palomitas'),
                         (3,'695844247','Coletas');
Insert INTO Realeza VALUES(1,'rey'),
                          (6,'princesa'),
                          (7,'reina');
Insert INTO Político VALUES(4,'Presidente', 'PSOE'),
                           (5,'Diputado','PACMA'),
                           (3,'Diputado','Podemos');
Insert INTO Evento VALUES(1,'Casa Real','2018/12/29,20',12,'Guardaespaldas'),
                         (2,'Wizink Centre','2019/20/16',21,'Amigo de la Infancia'),
                         (3,'Moncloa','2019/02/16,18','Agente del CNI');
Insert INTO presentan VALUES(1,3),
                            (2,1),
                            (3,2);

Select * FROM Contacto;
Select * FROM Famoso;
Select * FROM Realeza;
Select * FROM Político;
Select * FROM Evento; 
Select * FROM presentan;
