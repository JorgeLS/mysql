Insert INTO CLIENTES VALUES ('12345678A', 'Perico', 'De los Palotes', '648759251', 'perico@todo.com'),
                            ('11111111Z', 'Fulanito', 'De tal', '910283927', 'ful@nito.com');
Insert INTO TIENDA VALUES ('Gran Viá', 'Madrid', 'Madrid', 'C/Gran Viá 32', '915214521',
                            'Lunes', 'Viernes', 9, 18),
                          ('Castellana', 'Madrid', 'Madrid', 'C/Castellana 108', '913542847',
                            'Lunes', 'Sabado', 8, 15);
Insert INTO OPERADORAS VALUES ('Vodafone', 'Rojo', 99, 540, 'www.vodafone.com'),
                              ('Movistar', 'Azul', 98, 610, 'www.movistar.com'),
                              ('Orange','Naranja',97,660,'www.orange.com');
Insert INTO TARIFAS VALUES ('Koala', 'Vodafone', 4, 'GB', 200, 'Si'),
                           ('Ardilla', 'Vodafone', 3, 'GB', 300, 'Si'),
                           ('Cebra','Orange',2,'GB',250,'Si');

