Drop Database IF EXISTS Ejercicio2;

Create Database Ejercicio2;
Use Ejercicio2;

Create Table Tabla(Nombre VARCHAR(20), BBDD VARCHAR(20), PRIMARY KEY (Nombre, BBDD));

Create Table Campo(Nombre VARCHAR(20), Tipo VARCHAR(20), NombreTABLA VARCHAR(20), 
    PRIMARY KEY(Nombre), FOREIGN KEY(NombreTABLA) references Tabla(Nombre));

Show Tables;
