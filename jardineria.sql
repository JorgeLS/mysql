DROP DATABASE IF EXISTS jardineria2;

CREATE DATABASE jardineria2;

USE jardineria2;

CREATE TABLE Oficinas (
    CodigoOficina VARCHAR(10) NOT NULL,
    Ciudad VARCHAR(30) NOT NULL,
    Pais VARCHAR(50) NOT NULL,
    Region VARCHAR(50) DEFAULT NULL,
    CodigoPostal VARCHAR(10) NOT NULL,
    Telefono VARCHAR(20) NOT NULL,
    LineaDireccion1 VARCHAR(50) NOT NULL,
    LineaDireccion2 VARCHAR(50) DEFAULT NULL,
    PRIMARY KEY (CodigoOficina)
) engine=innodb;

CREATE TABLE Empleados (
    CodigoEmpleado INT NOT NULL,
    Nombre VARCHAR(50) NOT NULL,
    Apellido1 VARCHAR(50) NOT NULL,
    Apellido2 VARCHAR(50) DEFAULT NULL,
    Extension VARCHAR(10) NOT NULL,
    Email VARCHAR(100) NOT NULL,
    CodigoOficina VARCHAR(10) NOT NULL,
    CodigoJefe INT DEFAULT NULL,
    Puesto VARCHAR(50) DEFAULT NULL,
    PRIMARY KEY (CodigoEmpleado),
    CONSTRAINT Empleados_OficinasFK FOREIGN KEY (CodigoOficina) REFERENCES Oficinas (CodigoOficina),
    CONSTRAINT Empleados_EmpleadosFK FOREIGN KEY (CodigoJefe) REFERENCES Empleados (CodigoEmpleado)
) engine=innodb;

CREATE TABLE GamasProductos (
    Gama VARCHAR(50) NOT NULL,
    DescripcionTexto TEXT,
    DescripcionHTML TEXT,
    Imagen blob,
    PRIMARY KEY (Gama)
) engine=innodb;

CREATE TABLE Clientes (
    CodigoCliente INT NOT NULL,
    NombreCliente VARCHAR(50) NOT NULL,
    NombreContacto VARCHAR(30) DEFAULT NULL,
    ApellidoContacto VARCHAR(30) DEFAULT NULL,
    Telefono VARCHAR(15) NOT NULL,
    Fax VARCHAR(15) NOT NULL,
    LineaDireccion1 VARCHAR(50) NOT NULL,
    LineaDireccion2 VARCHAR(50) DEFAULT NULL,
    Ciudad VARCHAR(50) NOT NULL,
    Region VARCHAR(50) DEFAULT NULL,
    Pais VARCHAR(50) DEFAULT NULL,
    CodigoPostal VARCHAR(10) DEFAULT NULL,
    CodigoEmpleadoRepVentas INT DEFAULT NULL,
    LimiteCredito NUMERIC(15,2) DEFAULT NULL,
    PRIMARY KEY (CodigoCliente),
    CONSTRAINT Clientes_EmpleadosFK FOREIGN KEY (CodigoEmpleadoRepVentas) REFERENCES Empleados (CodigoEmpleado)
) engine=innodb;

CREATE TABLE Pedidos (
    CodigoPedido INT NOT NULL,
    FechaPedido DATE NOT NULL,
    FechaEsperada DATE NOT NULL,
    FechaEntrega DATE DEFAULT NULL,
    Estado VARCHAR(15) NOT NULL,
    Comentarios TEXT,
    CodigoCliente INT NOT NULL,
    PRIMARY KEY (CodigoPedido),
    CONSTRAINT Pedidos_Cliente FOREIGN KEY (CodigoCliente) REFERENCES Clientes (CodigoCliente)
) engine=innodb;

CREATE TABLE Productos (
    CodigoProducto VARCHAR(15) NOT NULL,
    Nombre VARCHAR(70) NOT NULL,
    Gama VARCHAR(50) NOT NULL,
    Dimensiones VARCHAR(25) NULL,
    Proveedor VARCHAR(50) DEFAULT NULL,
    Descripcion TEXT NULL,
    CantidadEnStock SMALLINT NOT NULL,
    PrecioVenta NUMERIC(15,2) NOT NULL,
    PrecioProveedor NUMERIC(15,2) DEFAULT NULL,
    PRIMARY KEY (CodigoProducto),
    CONSTRAINT Productos_gamaFK FOREIGN KEY (Gama) REFERENCES GamasProductos (Gama)
) engine=innodb;

CREATE TABLE DetallePedidos (
    CodigoPedido INT NOT NULL,
    CodigoProducto VARCHAR(15) NOT NULL,
    Cantidad INT NOT NULL,
    PrecioUnidad NUMERIC(15,2) NOT NULL,
    NumeroLinea SMALLINT NOT NULL,
    PRIMARY KEY (CodigoPedido,CodigoProducto),
    CONSTRAINT DetallePedidos_PedidoFK FOREIGN KEY (CodigoPedido) REFERENCES Pedidos (CodigoPedido),
    CONSTRAINT DetallePedidos_ProductoFK FOREIGN KEY (CodigoProducto) REFERENCES Productos (CodigoProducto)
)engine=innodb;

CREATE TABLE Pagos (
    CodigoCliente INT NOT NULL,
    FormaPago VARCHAR(40) NOT NULL,
    IDTransaccion VARCHAR(50) NOT NULL,
    FechaPago DATE NOT NULL,
    Cantidad NUMERIC(15,2) NOT NULL,
    PRIMARY KEY (CodigoCliente,IDTransaccion),
    CONSTRAINT Pagos_clienteFK FOREIGN KEY (CodigoCliente) REFERENCES Clientes (CodigoCliente)
) engine=innodb;

SOURCE jardineria-DATOS.sql
