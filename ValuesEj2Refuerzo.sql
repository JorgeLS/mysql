INSERT INTO ACTORES(Nombre, Fecha) VALUES
('Ewan MCGregor', '1971-03-31'),  ('Hayden Christensen', '1981-04-19'),
('Ian McDiarmid', '1944-08-11');

INSERT INTO  PERSONAJES(Nombre, Grado, Codigo_ACTORES, CodigoSuperior_PERSONAJES) VALUES
('ObiWan-KenoVee', 7,1,NULL),  ('Luke SkyWalker', 9, 2, 1),
('Emperador Palpatine', 10, 3, NULL);

INSERT INTO PLANETAS(Galaxia, Nombre) VALUES
('Alderaan','Alderaan'), ('Moddell','Endor'), ('Anoat','Hoth');

INSERT INTO PELICULAS(Titulo, Director, Año) VALUES
('Star Wars I: La Amenaza Fantasma', 'George Lucas', 1999),
('Star Wars IV: Una Nueva Esperanza', 'George Lucas', 1977),
('Star Wars II: El ataque de los Clones', 'George Lucas', 2002);

INSERT INTO PERSONAJESPELICULAS VALUES
(1, 2), (2 , 3), (3, 1);

INSERT INTO NAVES(NºTripulantes, Nombre) VALUES
(1, 'Ala-X'), (10, 'Alcón Milenario'), (1, 'Caza TIE');

INSERT INTO VISITAS VALUES
(NULL, 1, 3), (2, 2, 2), (NULL, 3, 1);

SELECT * FROM ACTORES;
SELECT * FROM PERSONAJES;
SELECT * FROM PLANETAS;
SELECT * FROM PELICULAS;
SELECT * FROM PERSONAJESPELICULAS;
SELECT * FROM NAVES;
SELECT * FROM VISITAS;

