/* Crea un procedimiento que te diga el mes */
DROP PROCEDURE IF EXISTS mes;

delimiter //
Create PROCEDURE mes (IN i INT)
BEGIN
    DECLARE s VARCHAR(30);
    /*
    Con IF
    IF i = 1 THEN SET s = 'Enero';
    ELSEIF i = 2 THEN SET s = 'Febrero';
    ELSEIF i = 3 THEN SET s = 'Marzo';
    ELSEIF i = 4 THEN SET s = 'Abril';
    ELSEIF i = 5 THEN SET s = 'Mayo';
    ELSEIF i = 6 THEN SET s = 'Junio';
    ELSEIF i = 7 THEN SET s = 'Julio';
    ELSEIF i = 8 THEN SET s = 'Agosto';
    ELSEIF i = 9 THEN SET s = 'Septiembre';
    ELSEIF i = 10 THEN SET s = 'Octubre';
    ELSEIF i = 11 THEN SET s = 'Noviembre';
    ELSE SET s = "Diciembre";
END IF;
*/
llamada: LOOP
  CASE i
    WHEN 1 THEN SET s = 'Enero';
    WHEN 2 THEN SET s = 'Febrero';
    WHEN 3 THEN SET s = 'Marzo';
    WHEN 4 THEN SET s = 'Abril';
    WHEN 5 THEN SET s = 'Mayo';
    WHEN 6 THEN SET s = 'Junio';
    WHEN 7 THEN SET s = 'Julio';
    WHEN 8 THEN SET s = 'Agosto';
    WHEN 9 THEN SET s = 'Septiembre';
    WHEN 10 THEN SET s = 'Octubre';
    WHEN 11 THEN SET s = 'Noviembre';
    WHEN 12 THEN SET s = 'Diciembre';
    ELSE
        BEGIN
            SET s = 'Introduce un mes de 1 a 12';
        END;
  END CASE;

  SET i = i + 1;
  IF i < 13 THEN ITERATE llamada;
  END IF;
  LEAVE llamada;
  Select (s) AS 'MES';
END LOOP llamada;
END; //
delimiter ;

call mes(2);
