/* Ejecuta un procedimiento que crea una tabla con informacion de la base de datos (Num clientes, Num Empleados, Num Pedidos) */
DROP PROCEDURE IF EXISTS crearInfo;

delimiter //
CREATE PROCEDURE crearInfo ()
  BEGIN
    DECLARE clientes INT;
    DECLARE empleados INT;
    DECLARE pedidos INT;

    DROP TABLE IF EXISTS Info;
    CREATE TABLE Info(NumClientes INT, NumEmpleados INT, NumPedidos INT);

    SET clientes = (SELECT COUNT(*) FROM Clientes);
    SET empleados = (SELECT COUNT(*) FROM Empleados);
    SET pedidos = (SELECT COUNT(*) FROM Pedidos);

    INSERT INTO Info VALUES (clientes, empleados, pedidos);
    /*
    INSERT INTO Info VALUES (
        (SELECT COUNT(*) FROM Clientes),
        (SELECT COUNT(*) FROM Empleados),
        (SELECT COUNT(*) FROM Pedidos)
    );
    */
  END; //

delimiter ;

CALL crearInfo();
SELECT * FROM Info;


/*
Trigger
 */
