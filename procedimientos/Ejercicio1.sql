/* Crea un procedimiento que devuelve un String con un concat */
DROP PROCEDURE IF EXISTS hola;

delimiter //
CREATE PROCEDURE hola (IN param1 CHAR(20))
BEGIN
    SELECT (CONCAT('hello, ', param1));
End //
delimiter ;

call hola('ohla');
