Drop Database IF EXISTS Ej3Repaso;

Create Database Ej3Repaso;
Use Ej3Repaso;

Create Table SOCIOS(NºSocio INT AUTO_INCREMENT NOT NULL, Nombre VARCHAR(20),
    Direccion VARCHAR(30), NumeroTelf VARCHAR(15), FechaInscripcion DATE,
    CONSTRAINT PK_SOCIOS PRIMARY KEY(NºSocio));

Create Table LIBROS(NºLibro BIGINT AUTO_INCREMENT, Titulo VARCHAR(30), Autor VARCHAR(20),
    FechaEdit DATE,
    CONSTRAINT PK_LIBROS PRIMARY KEY(NºLibro));

Create Table PRESTAMOS(ID INT AUTO_INCREMENT, NºSocio INT, NºLibro BIGINT, FechaRetiro DATE,
    FechaEntrega DATE,
    CONSTRAINT PK_PRESTAMOS PRIMARY KEY(ID));

Show Tables;

ALTER TABLE PRESTAMOS ADD (
    CONSTRAINT FK_SOCIOS_PRESTAMOS FOREIGN KEY(NºSocio) REFERENCES SOCIOS(NºSocio),
    CONSTRAINT FK_LIBROS_PRESTAMOS FOREIGN KEY(NºLibro) REFERENCES LIBROS(NºLibro));

INSERT INTO SOCIOS(Nombre, Direccion, NumeroTelf, FechaInscripcion) VALUES
('Pedro Gil','Canelones','123.456.789','1984-12-12'),
('Jose M. FORO','Montevideo','987.654.321','1997-03-02'),
('Elba Lazo','Las Piedras','666.777.888','2000-06-02'),
('Marta Cana','Montevideo','600.200.200','1999-08-15');

INSERT INTO LIBROS(Titulo, Autor, FechaEdit) VALUES
('El Meteorologo','Aitor Menta','1954-12-12'),
('La fiesta','Encarna Vales','1987-03-02'),
('El Golpe','Marcos Corro','1990-06-02'),
('La Furia','Elbio Lento','1994-12-25');

INSERT INTO PRESTAMOS(NºSocio, NºLibro, FechaRetiro, FechaEntrega) VALUES
('1','1','2003-12-12','2003-12-22'),
('2','1','2003-02-02','2003-02-12'),
('3','2','2003-02-02','2003-02-12'),
('4','4','2003-08-03','2003-08-13');

SELECT * FROM SOCIOS;
SELECT * FROM LIBROS;
SELECT * FROM PRESTAMOS;

DELETE FROM SOCIOS WHERE NºSocio=4;
NO se puede borrar ya que es una PRIMARY KEY que a su vez es una FOREIGN KEY en PRESTAMOS

SELECT * FROM PRESTAMSO WHERE ID 
