/* Crea una funcion que te diga el mes */
DROP FUNCTION IF EXISTS Mes;

DELIMITER //

CREATE FUNCTION mes(num INT)
  RETURNS CHAR(20)
  BEGIN
    DECLARE s CHAR(20);

    IF num = 1 THEN SET s = 'Enero';
    ELSEIF num = 2 THEN SET s = 'Febrero';
    ELSEIF num = 3 THEN SET s = 'Marzo';
    ELSEIF num = 4 THEN SET s = 'Abril';
    ELSEIF num = 5 THEN SET s = 'Mayo';
    ELSEIF num = 6 THEN SET s = 'Junio';
    ELSEIF num = 7 THEN SET s = 'Julio';
    ELSEIF num = 8 THEN SET s = 'Agosto';
    ELSEIF num = 9 THEN SET s = 'Septiembre';
    ELSEIF num = 10 THEN SET s = 'Octubre';
    ELSEIF num = 11 THEN SET s = 'Noviembre';
    ELSEIF num = 12 THEN SET s = 'Diciembre';
    ELSE SET s = 'No hay mes';
    END IF;

    RETURN CONCAT('Mes: ', s);
  END //

DELIMITER ;

SELECT mes(12) AS 'MES';
SELECT mes(1), mes(2), mes(3), mes(4), mes(5), mes(6);
