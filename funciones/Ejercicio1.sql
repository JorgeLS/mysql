/* Crea una funcion que devuelve un String con un concat */
DROP FUNCTION IF EXISTS hola;

CREATE FUNCTION hola(s CHAR(20))
  RETURNS CHAR(20)
  RETURN CONCAT('HELLO, ', s, '!');

SELECT hola('Jorge');
